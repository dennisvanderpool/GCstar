
"%PERL%\site\bin\pp.bat" -o gcstar.exe -B gcstar ^
 -gui ^
 -M locale ^
 -M filetest ^
 -M threads -M threads::shared ^
 -M JSON ^
 -M Glib::Object::Subclass -M Gtk2::SimpleList -M Gtk2::Gdk::Keysyms ^
 -M Locale::Country ^
 -M MIME::Base64 -M Unicode::Normalize ^
 -M LWP -M LWP::Simple -M LWP::UserAgent -M Net::SMTP -M URI::Escape ^
 -M HTTP::Cookies::Netscape -M HTML::Entities -M Text::Wrap -M XML::Simple ^
 -M DateTime::Format::Strptime -M Date::Calc -M Time::Piece ^
 -M Image::ExifTool ^
 -M MP3::Info -M MP3::Tag ^
 -M Archive::Tar -M Archive::Zip -M Compress::Zlib -M Digest::MD5 ^
 -M FindBin ^
 -M GD -M GD::Text -M GD::Graph::bars -M GD::Graph::area -M GD::Graph::pie ^
 -M XSLoader ^
 -vvv

rem -a "..\lib\gcstar;lib" ^
rem -a "..\share\gcstar;share" ^

rem "%PERL%\bin\perl" change_gcstar_icon.pl

rem dir

rem --link="%PERL%\site\lib\auto\Glib\Glib.dll" ^
rem --link="%PERL%\site\lib\auto\Gtk2\Gtk2.dll" ^
rem --link="%PERL%\site\lib\auto\Cairo\Cairo.dll" ^
rem -M Net::FreeDB ^
rem -M Ogg::Vorbis::Header::PurePerl -M Getopt::Long ^
rem --icon=..\share\gcstar\icons\GCstar.ico ^
rem --info="ProductName=GCstar;ProductVersion=1.7.2;FileVersion=1.7.2;FileDescription=GCstar, Personal Collections Manager;Comments=http://www.gcstar.org;LegalCopyright=GNU GPL" ^
