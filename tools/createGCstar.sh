#! bash
# 
# createGCstar.sh
#
#    creates a package executable with GCstar, Perl, Gtk2 and other libraries

cd ../gcstar/bin

pp -o gcstar.exe -B gcstar.pl\
 -gui\
 -M locale\
 -M filetest\
 -M threads -M threads::shared\
 -M JSON\
 -M Gtk2\
 -M Glib::Object::Subclass -M Gtk2::SimpleList -M Gtk2::Gdk::Keysyms\
 -M Locale::Country\
 -M MIME::Base64 -M Unicode::Normalize\
 -M LWP -M LWP::Simple -M LWP::UserAgent -M Net::SMTP -M URI::Escape\
 -M HTTP::Cookies::Netscape -M HTML::Entities -M Text::Wrap -M XML::Simple\
 -M DateTime::Format::Strptime -M Date::Calc -M Time::Piece\
 -M Image::ExifTool\
 -M MP3::Info -M MP3::Tag\
 -M Archive::Tar -M Archive::Zip -M Compress::Zlib -M Digest::MD5\
 -M FindBin\
 -M GD -M GD::Text -M GD::Graph::bars -M GD::Graph::area -M GD::Graph::pie\
 -a "../lib/gcstar;lib"\
 -a "../share/gcstar;share"\
 -vvv

# --link="$PERL/auto/Glib/Glib.so"\
# --link="$PERL/auto/Gtk2/Gtk2.so"\
# --link="$PERL/auto/Cairo/Cairo.so"\
