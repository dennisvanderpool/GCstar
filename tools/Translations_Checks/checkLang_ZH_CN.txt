

Lang ZH_CN
-------

Errors:


GCModels/GCmusiccourses.pm 'Artist' => 'Artist',
GCModels/GCmusiccourses.pm 'Artists' => 'Artists',
GCModels/GCmusiccourses.pm 'Audio' => 'Audio ',
GCModels/GCmusiccourses.pm 'Audio1' => 'Audio #1',
GCModels/GCmusiccourses.pm 'Audio2' => 'Audio #2',
GCModels/GCmusiccourses.pm 'Audio3' => 'Audio #3',
GCModels/GCmusiccourses.pm 'Audio4' => 'Audio #4',
GCModels/GCmusiccourses.pm 'Audio5' => 'Audio #5',
GCModels/GCmusiccourses.pm 'Audio6' => 'Audio #6',
GCModels/GCmusiccourses.pm 'Audio7' => 'Audio #7',
GCModels/GCmusiccourses.pm 'Audio8' => 'Audio #8',
GCModels/GCmusiccourses.pm 'Audio9' => 'Audio #9',
GCModels/GCmusiccourses.pm 'AudioMissing' => 'Missing audio',
GCModels/GCmusiccourses.pm 'CollectionDescription' => 'Music courses collection',
GCModels/GCmusiccourses.pm 'Comment' => 'Comments',
GCModels/GCmusiccourses.pm 'Date' => 'Publication date',
GCModels/GCmusiccourses.pm 'Details' => 'Details ',
GCModels/GCmusiccourses.pm 'Director' => 'Director',
GCModels/GCmusiccourses.pm 'Directors' => 'Directors',
GCModels/GCmusiccourses.pm 'Doc' => 'Documentation ',
GCModels/GCmusiccourses.pm 'Doc1' => 'Document #1',
GCModels/GCmusiccourses.pm 'Doc2' => 'Document #2',
GCModels/GCmusiccourses.pm 'Doc3' => 'Document #3',
GCModels/GCmusiccourses.pm 'Doc4' => 'Document #4',
GCModels/GCmusiccourses.pm 'Doc5' => 'Document #5',
GCModels/GCmusiccourses.pm 'Doc6' => 'Document #6',
GCModels/GCmusiccourses.pm 'Doc7' => 'Document #7',
GCModels/GCmusiccourses.pm 'Doc8' => 'Document #8',
GCModels/GCmusiccourses.pm 'Doc9' => 'Document #9',
GCModels/GCmusiccourses.pm 'DocMissing' => 'Missing documentation',
GCModels/GCmusiccourses.pm 'Exercise' => 'Exercise',
GCModels/GCmusiccourses.pm 'Exercises' => 'Exercises',
GCModels/GCmusiccourses.pm 'FilterSeenNo' => '_Not Yet Viewed',
GCModels/GCmusiccourses.pm 'FilterSeenYes' => '_Already Viewed',
GCModels/GCmusiccourses.pm 'Format' => 'Format',
GCModels/GCmusiccourses.pm 'Genre' => 'Genre',
GCModels/GCmusiccourses.pm 'Genres' => 'Genres',
GCModels/GCmusiccourses.pm 'ISBN10' => 'ISBN10',
GCModels/GCmusiccourses.pm 'ISBN13' => 'ISBN13',
GCModels/GCmusiccourses.pm 'Id' => 'Id',
GCModels/GCmusiccourses.pm 'Identification' => 'Identification ',
GCModels/GCmusiccourses.pm 'Identifier' => 'Publisher Id',
GCModels/GCmusiccourses.pm 'Image' => 'Front cover',
GCModels/GCmusiccourses.pm 'Instrument' => 'Instrument',
GCModels/GCmusiccourses.pm 'Instruments' => 'Instruments',
GCModels/GCmusiccourses.pm 'Items' => 'HASH(0x87a6998)',
GCModels/GCmusiccourses.pm 'Key' => 'Key',
GCModels/GCmusiccourses.pm 'Keys' => 'Keys',
GCModels/GCmusiccourses.pm 'LabelDVD' => 'DVD Number',
GCModels/GCmusiccourses.pm 'Level' => 'Level',
GCModels/GCmusiccourses.pm 'Levels' => 'Levels',
GCModels/GCmusiccourses.pm 'Main' => 'Main',
GCModels/GCmusiccourses.pm 'MissingNo' => 'Present',
GCModels/GCmusiccourses.pm 'MissingYes' => 'Missing',
GCModels/GCmusiccourses.pm 'Name' => 'Name',
GCModels/GCmusiccourses.pm 'NewItem' => 'New music course',
GCModels/GCmusiccourses.pm 'Path' => 'Path',
GCModels/GCmusiccourses.pm 'Performer' => 'Performer',
GCModels/GCmusiccourses.pm 'Performers' => 'Performer',
GCModels/GCmusiccourses.pm 'Publisher' => 'Publisher',
GCModels/GCmusiccourses.pm 'Publishers' => 'Publishers',
GCModels/GCmusiccourses.pm 'Review' => 'Review ',
GCModels/GCmusiccourses.pm 'Seen' => 'Viewed',
GCModels/GCmusiccourses.pm 'SeenNo' => 'Not viewed',
GCModels/GCmusiccourses.pm 'SeenYes' => 'Viewed',
GCModels/GCmusiccourses.pm 'Series' => 'Series',
GCModels/GCmusiccourses.pm 'Song' => 'Song',
GCModels/GCmusiccourses.pm 'Songs' => 'Songs',
GCModels/GCmusiccourses.pm 'SourceUrl' => 'Source',
GCModels/GCmusiccourses.pm 'Synopsis' => 'Synopsis',
GCModels/GCmusiccourses.pm 'Tab' => 'Tablatures ',
GCModels/GCmusiccourses.pm 'Tab1' => 'Tab #1',
GCModels/GCmusiccourses.pm 'Tab2' => 'Tab #2',
GCModels/GCmusiccourses.pm 'Tab3' => 'Tab #3',
GCModels/GCmusiccourses.pm 'Tab4' => 'Tab #4',
GCModels/GCmusiccourses.pm 'Tab5' => 'Tab #5',
GCModels/GCmusiccourses.pm 'Tab6' => 'Tab #6',
GCModels/GCmusiccourses.pm 'Tab7' => 'Tab #7',
GCModels/GCmusiccourses.pm 'Tab8' => 'Tab #8',
GCModels/GCmusiccourses.pm 'Tab9' => 'Tab #9',
GCModels/GCmusiccourses.pm 'TabMissing' => 'Missing tabs',
GCModels/GCmusiccourses.pm 'Technique' => 'Technique',
GCModels/GCmusiccourses.pm 'Techniques' => 'Techniques',
GCModels/GCmusiccourses.pm 'Time' => 'Length',
GCModels/GCmusiccourses.pm 'Title' => 'Title',
GCModels/GCmusiccourses.pm 'Tuning' => 'Tuning',
GCModels/GCmusiccourses.pm 'Tunings' => 'Tunings',
GCModels/GCmusiccourses.pm 'UPC' => 'UPC',
GCModels/GCmusiccourses.pm 'Url' => 'Web page',
GCModels/GCmusiccourses.pm 'Video' => 'Video ',
GCModels/GCmusiccourses.pm 'Video1' => 'Video #1',
GCModels/GCmusiccourses.pm 'Video2' => 'Video #2',
GCModels/GCmusiccourses.pm 'Video3' => 'Video #3',
GCModels/GCmusiccourses.pm 'Video4' => 'Video #4',
GCModels/GCmusiccourses.pm 'Video5' => 'Video #5',
GCModels/GCmusiccourses.pm 'Video6' => 'Video #6',
GCModels/GCmusiccourses.pm 'Video7' => 'Video #7',
GCModels/GCmusiccourses.pm 'Video8' => 'Video #8',
GCModels/GCmusiccourses.pm 'Video9' => 'Video #9',
GCModels/GCmusiccourses.pm 'Volume' => 'Volume',
GCModels/GCmusiccourses.pm 'videoMissing' => 'Missing video',

Warnings:

GCstar.pm 'BorrowersEmail' => 'E-mail',
GCstar.pm 'DefaultValuesTip' => 'Values set in this window will be used as the default values when creating a new {lowercase1}',
GCstar.pm 'ImagesOptionsAnimateImgList' => 'Use animations',
GCstar.pm 'ImportOverwriteItems' => 'Overwrite similar items',
GCstar.pm 'OptionsBackup' => 'Backup collection file',
GCstar.pm 'OptionsPicturesFormatInternal' => 'gcstar__',
GCstar.pm 'PluginsLogo' => 'Logo',
GCstar.pm 'PropertiesEmail' => 'Email',
GCstar.pm 'Separator' => ': ',

GCExport/GCExportPDB.pm 'Name' => 'Palm PDB',
GCImport/GCImportCSV.pm 'FieldsWithQuotes' => 'Fields with quotes',
GCImport/GCImportScanner.pm 'EAN' => 'Barcode',
GCImport/GCImportScanner.pm 'Local' => 'Local (used as a keyboard)',
GCImport/GCImportScanner.pm 'Name' => 'Barcode scanner',
GCImport/GCImportScanner.pm 'Network' => 'Network (eg: GCstar Scanner for Android)',
GCImport/GCImportScanner.pm 'NothingFound' => 'Nothing was found for "%s". An empty item will be added.',
GCImport/GCImportScanner.pm 'Plugin' => 'Site to be used',
GCImport/GCImportScanner.pm 'Port' => 'Port to listen on',
GCImport/GCImportScanner.pm 'Previous' => '"%s" will be added.',
GCImport/GCImportScanner.pm 'ScanOtherPrompt' => 'Scan another item or press on Done',
GCImport/GCImportScanner.pm 'ScanPrompt' => 'Scan an item or press on Done',
GCImport/GCImportScanner.pm 'Terminate' => 'Done',
GCImport/GCImportScanner.pm 'Type' => 'Scanner type',
GCImport/GCImportScanner.pm 'UseFirst' => 'Select first one if many results',
GCImport/GCImportScanner.pm 'Waiting' => 'Waiting for barcode',
GCModels/GCTVepisodes.pm 'General' => 'General',
GCModels/GCTVepisodes.pm 'Id' => 'Id',
GCModels/GCTVepisodes.pm 'Writer' => 'Writer',
GCModels/GCboardgames.pm 'Id' => 'Id',
GCModels/GCbooks.pm 'Isbn' => 'ISBN',
GCModels/GCbuildingtoys.pm 'Box' => 'Box',
GCModels/GCbuildingtoys.pm 'Brand' => 'Brand',
GCModels/GCbuildingtoys.pm 'CollectionDescription' => 'Building toys collection',
GCModels/GCbuildingtoys.pm 'Comments' => 'Comments',
GCModels/GCbuildingtoys.pm 'Description' => 'Description',
GCModels/GCbuildingtoys.pm 'Details' => 'Details',
GCModels/GCbuildingtoys.pm 'EstimatedPrice' => 'Current estimate',
GCModels/GCbuildingtoys.pm 'Id' => 'Id',
GCModels/GCbuildingtoys.pm 'Images' => 'Images',
GCModels/GCbuildingtoys.pm 'Instructions' => 'Electronic instructions',
GCModels/GCbuildingtoys.pm 'MainPic' => 'Main picture',
GCModels/GCbuildingtoys.pm 'Manual' => 'Instructions manual',
GCModels/GCbuildingtoys.pm 'Minifigures' => 'Minifigures',
GCModels/GCbuildingtoys.pm 'MinifiguresPics' => 'Pictures of minifigures',
GCModels/GCbuildingtoys.pm 'Name' => 'Name',
GCModels/GCbuildingtoys.pm 'NbOwned' => 'Number owned',
GCModels/GCbuildingtoys.pm 'NewItem' => 'New set',
GCModels/GCbuildingtoys.pm 'NumberMiniFigs' => 'Number of minifigures',
GCModels/GCbuildingtoys.pm 'NumberPieces' => 'Number of pieces',
GCModels/GCbuildingtoys.pm 'PaidPrice' => 'Price paid',
GCModels/GCbuildingtoys.pm 'Pictures' => 'Pictures',
GCModels/GCbuildingtoys.pm 'Reference' => 'Reference',
GCModels/GCbuildingtoys.pm 'Released' => 'Release Date',
GCModels/GCbuildingtoys.pm 'Sealed' => 'Sealed',
GCModels/GCbuildingtoys.pm 'SetDescription' => 'Set description',
GCModels/GCbuildingtoys.pm 'Theme' => 'Theme',
GCModels/GCbuildingtoys.pm 'Url' => 'Web page',
GCModels/GCcoins.pm 'Anniversary coin' => 'Anniversary coin',
GCModels/GCcoins.pm 'Axis' => 'Axis',
GCModels/GCcoins.pm 'Back' => 'Back',
GCModels/GCcoins.pm 'Bring' => 'Bring',
GCModels/GCcoins.pm 'Buyed' => 'Bought',
GCModels/GCcoins.pm 'Calendar' => 'Calendar',
GCModels/GCcoins.pm 'Catalogue1' => 'Catalogue1',
GCModels/GCcoins.pm 'Catalogue2' => 'Catalogue2',
GCModels/GCcoins.pm 'City letter' => 'City letter',
GCModels/GCcoins.pm 'City' => 'City',
GCModels/GCcoins.pm 'Depth' => 'Depth (mm)',
GCModels/GCcoins.pm 'Edge type' => 'Edge type',
GCModels/GCcoins.pm 'Form' => 'Form',
GCModels/GCcoins.pm 'Found' => 'Found',
GCModels/GCcoins.pm 'Front' => 'Front',
GCModels/GCcoins.pm 'Gift' => 'Gift',
GCModels/GCcoins.pm 'Grade1' => 'BS-1',
GCModels/GCcoins.pm 'Grade10' => 'VG-10',
GCModels/GCcoins.pm 'Grade12' => 'F-12',
GCModels/GCcoins.pm 'Grade15' => 'F-15',
GCModels/GCcoins.pm 'Grade2' => 'FR-2',
GCModels/GCcoins.pm 'Grade20' => 'VF-20',
GCModels/GCcoins.pm 'Grade25' => 'VF-25',
GCModels/GCcoins.pm 'Grade3' => 'AG-3',
GCModels/GCcoins.pm 'Grade30' => 'VF-30',
GCModels/GCcoins.pm 'Grade35' => 'VF-35',
GCModels/GCcoins.pm 'Grade4' => 'G-4',
GCModels/GCcoins.pm 'Grade40' => 'XF-40',
GCModels/GCcoins.pm 'Grade45' => 'XF-45',
GCModels/GCcoins.pm 'Grade50' => 'AU-50',
GCModels/GCcoins.pm 'Grade53' => 'AU-53',
GCModels/GCcoins.pm 'Grade55' => 'AU-55',
GCModels/GCcoins.pm 'Grade58' => 'AU-58',
GCModels/GCcoins.pm 'Grade6' => 'G-6',
GCModels/GCcoins.pm 'Grade60' => 'MS-60',
GCModels/GCcoins.pm 'Grade61' => 'MS-61',
GCModels/GCcoins.pm 'Grade62' => 'MS-62',
GCModels/GCcoins.pm 'Grade63' => 'MS-63',
GCModels/GCcoins.pm 'Grade64' => 'MS-64',
GCModels/GCcoins.pm 'Grade65' => 'MS-65',
GCModels/GCcoins.pm 'Grade66' => 'MS-66',
GCModels/GCcoins.pm 'Grade67' => 'MS-67',
GCModels/GCcoins.pm 'Grade68' => 'MS-68',
GCModels/GCcoins.pm 'Grade69' => 'MS-69',
GCModels/GCcoins.pm 'Grade70' => 'MS-70',
GCModels/GCcoins.pm 'Grade8' => 'VG-8',
GCModels/GCcoins.pm 'Location' => 'Location',
GCModels/GCcoins.pm 'Medal' => 'Medal',
GCModels/GCcoins.pm 'Mint mark' => 'Mint mark',
GCModels/GCcoins.pm 'Mint' => 'Mint',
GCModels/GCcoins.pm 'Mintmaster mark' => 'Mintmaster mark',
GCModels/GCcoins.pm 'Mintmaster' => 'Mintmaster',
GCModels/GCcoins.pm 'Monetary' => 'Monetary (180�)',
GCModels/GCcoins.pm 'Non circulating coin' => 'Non circulating coin',
GCModels/GCcoins.pm 'Number' => 'Number',
GCModels/GCcoins.pm 'Number1' => 'Number1',
GCModels/GCcoins.pm 'Number2' => 'Number2',
GCModels/GCcoins.pm 'Price' => 'Price',
GCModels/GCcoins.pm 'Quantity' => 'Quantity',
GCModels/GCcoins.pm 'Series' => 'Series',
GCModels/GCcoins.pm 'Token' => 'Token',
GCModels/GCcoins.pm 'Turn' => 'Turn',
GCModels/GCcoins.pm 'Weight' => 'Weight (g)',
GCModels/GCcoins.pm 'Years of coinage' => 'Years of coinage',
GCModels/GCcomics.pm 'Artist' => 'Cover Artist',
GCModels/GCcomics.pm 'ISBN' => 'ISBN',
GCModels/GCcomics.pm 'Id' => 'Id',
GCModels/GCcomics.pm 'Inker' => 'Inker',
GCModels/GCcomics.pm 'Letterer' => 'Letterer',
GCModels/GCfilms.pm 'Id' => 'Id',
GCModels/GCgadgets.pm 'Brand' => 'Brand',
GCModels/GCgadgets.pm 'Characteristics' => 'Characteristic',
GCModels/GCgadgets.pm 'CollectionDescription' => 'Gadgets collection',
GCModels/GCgadgets.pm 'Description' => 'Description',
GCModels/GCgadgets.pm 'Details' => 'Details',
GCModels/GCgadgets.pm 'EAN' => 'EAN',
GCModels/GCgadgets.pm 'General' => 'General',
GCModels/GCgadgets.pm 'Genres' => 'Genres',
GCModels/GCgadgets.pm 'Image' => 'Image',
GCModels/GCgadgets.pm 'Location' => 'Location',
GCModels/GCgadgets.pm 'NewItem' => 'New gadget',
GCModels/GCgadgets.pm 'Notes' => 'Notes',
GCModels/GCgadgets.pm 'Origin' => 'Origin',
GCModels/GCgadgets.pm 'Price' => 'Price',
GCModels/GCgadgets.pm 'PurchaseDate' => 'Purchase date',
GCModels/GCgadgets.pm 'Quantity' => 'Quantity',
GCModels/GCgadgets.pm 'Title' => 'Item name',
GCModels/GCgadgets.pm 'UPC' => 'UPC',
GCModels/GCgadgets.pm 'Url' => 'Web page',
GCModels/GCgames.pm 'Ean' => 'EAN',
GCModels/GCgames.pm 'Id' => 'Id',
GCModels/GCgames.pm 'SizeUnit' => 'Size unit',
GCModels/GCminicars.pm 'BOX' => 'Box',
GCModels/GCmusics.pm 'Unique' => 'ISRC/EAN',
GCModels/GCsmartcards.pm 'Catalog1' => 'Phonecote / Infopuce (YT)',
GCModels/GCsmartcards.pm 'Catalog2' => 'La Cote en Poche',
GCModels/GCsmartcards.pm 'Quotationnew30' => 'Cotation Autre',
GCModels/GCsoftware.pm 'Ean' => 'EAN',
GCModels/GCsoftware.pm 'Id' => 'Id',
